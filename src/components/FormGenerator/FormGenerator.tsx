import { fhirR4 } from '@smile-cdr/fhirts';
import React, { FC, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useForm } from 'react-hook-form';


interface FormGeneratorProps {
  questionnaire?: fhirR4.Questionnaire;
}

const FormGenerator: FC<FormGeneratorProps> = ({ questionnaire = {} }) => {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [QuestionnaireResponse, setQuestionnaireResponse] = useState({});
  const [QuestionnaireResponseResponse, setQuestionnaireResponseResponse] = useState("");
  const [ValidationResponse, setValidationResponse] = useState("");
  const onSubmit = (data: {
    [key: string]: string;
  }) => {
    const QuestionnaireResponse = new fhirR4.QuestionnaireResponse();
    const originalQ = questionnaire as fhirR4.Questionnaire;
    QuestionnaireResponse.resourceType = "QuestionnaireResponse";
    QuestionnaireResponse.questionnaire = originalQ.url;
    QuestionnaireResponse.status = 'completed';
    QuestionnaireResponse.item = Object.entries(data).map(([key, val]) => {
      const originalItem = originalQ.item?.find(item => item.linkId === key) || {};
      const QuestionnaireResponseitem = new fhirR4.QuestionnaireResponseItem();
      QuestionnaireResponseitem.linkId = key || "";
      switch (originalItem.type) {
        case "string":
          QuestionnaireResponseitem.answer = [{
            valueString: val || "none"
          }];
          break;
        case "choice":
          QuestionnaireResponseitem.answer = originalItem?.answerOption?.[0]?.valueCoding ? [{
            valueCoding: originalItem?.answerOption?.find(item => item.valueCoding?.code === val)?.valueCoding || {}
          }] : [{
            valueString: val || "none"
          }];
          break;
        default:
          break;
      }
      return QuestionnaireResponseitem;
    });
    setQuestionnaireResponse(QuestionnaireResponse);
  };
  const postQuestionnaireResponse = () => {
    const headers = new Headers();
    headers.set("Authorization", `Basic ${btoa("admin:password")}`);
    headers.set("Content-Type", `application/fhir+json`);
    fetch("http://localhost:8000/QuestionnaireResponse", {
      method: "POST",
      headers: headers,
      body: JSON.stringify(QuestionnaireResponse)
    })
      .then(resp => resp.json())
      .catch(resp => {
        setQuestionnaireResponseResponse("Failed!");
        setValidationResponse("");
        console.log(resp);
      })
      .then(resp => {
        setQuestionnaireResponseResponse("Success! ID: " + resp.id);
        // setValidationResponse(JSON.stringify(resp.meta.tag[0], null, 2));
        console.log(resp);
      })
      .catch(resp => {
        setQuestionnaireResponseResponse("Failed!");
        setValidationResponse("");
        console.log(resp);
      });
  };
  // async function generateFromValueSet(data): Promse<any> {
  //   const headers = new Headers();
  //   headers.set("Authorization", `Basic ${btoa("admin:password")}`);
  //   return fetch(data, {
  //     method: "GET",
  //     headers: headers
  //   }).then(resp => resp.json())
  //     .then(resp => {
  //       return resp.compose.include.map((concept) => (
  //         <option value={concept.code}>{concept.display || concept.code}</option>
  //       ));
  //     });
  // }
  return <>
    {Object.keys(QuestionnaireResponse).length ? <Button onClick={postQuestionnaireResponse} variant="primary">Post</Button> : ""}
    {QuestionnaireResponseResponse && <span className={QuestionnaireResponseResponse === 'Failed!' ? "red p-2 m-2 small" : "green p-2 m-2 small"}>{QuestionnaireResponseResponse}</span>}
    {ValidationResponse && <pre className="green p-2 m-2 small">{ValidationResponse}</pre>}

    <Form onSubmit={handleSubmit(onSubmit)}>
      {questionnaire && questionnaire.item?.map(item => {
        switch (item.type) {
          case "string":
            return (<Form.Group key={item.linkId} className="my-4">
              <Form.Label>{item.text}</Form.Label>
              <Form.Control type={'string'} id={item.linkId} {...register(item.linkId || "", { required: item.required })} />
            </Form.Group>);
            break;
          case "choice":
            return (<Form.Group key={item.linkId} className="my-4">
              <Form.Label>{item.text}</Form.Label>
              <Form.Select id={item.linkId} {...register(item.linkId || "", { required: item.required })} >
                <option value="none">Select option</option>
                {
                  item.answerOption?.map((value, index) => (
                    <option key={item.linkId + "_" + index} value={value.valueCoding ? value.valueCoding.code : value.valueString}>{value.valueCoding ? (value.valueCoding.display ? value.valueCoding.display : value.valueCoding.code) : value.valueString}</option>
                  ))

                }
              </Form.Select>
            </Form.Group>);
            break;
          default:
            break;
        }
      })}
      {questionnaire && <Button className="my-2" type={"submit"} >Prepare QuestionnaireResponse to Post</Button>}
    </Form>
    {Object.keys(QuestionnaireResponse).length ? <pre className="tiny">{JSON.stringify(QuestionnaireResponse, null, 2)}</pre> : ""}
  </>;
};

export default FormGenerator;
