import React, { useState } from 'react';
import FormGenerator from './components/FormGenerator/FormGenerator';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useForm } from "react-hook-form";
import { Button, Col, Container, Form, Row } from 'react-bootstrap';

function App() {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [questionnaire, setQuestionnaire] = useState();
  const [authorized, setAuthorized] = useState(true);
  const [error, setError] = useState('');
  const onSubmit = (data: any) => {
    const headers = new Headers();
    headers.set("Authorization", `Basic ${btoa("admin:password")}`);
    // headers.set("Authorization", `Bearer ${token}`);
    fetch(data.questionnaireUrl, {
      method: "GET",
      headers: headers
    }).then(resp => resp.json())
      .then(resp => setQuestionnaire(resp));
  };
  return (

    <Container className='App-header fluid'>
      {error && <span style={{ color: 'red' }}>{error}</span>}
      {authorized ?
        <Row>
          <Col>
            <h1>Questionnaire</h1>
            <FormGenerator questionnaire={questionnaire} />
          </Col>
          <Col>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Form.Control type="string" className="my-2" defaultValue="http://localhost:8000/Questionnaire/sample7" {...register("questionnaireUrl")} />
              <Button className="my-2" type="submit" >Get Questionnaire</Button>
            </form>
            {questionnaire && <pre className="tiny">{JSON.stringify(questionnaire, null, 2)}</pre>}
          </Col>
        </Row>

        :
        // <OAuth2Login
        //   authorizationUrl={"http://"}
        //   clientId={"demoId"}
        //   redirectUri={"redirectUri"}
        //   responseType="token"
        //   buttonText="Implicit grant login"
        //   onSuccess={() => setAuthorized(true)}
        //   onFailure={() => setError('login failed')}
        // />
        null
      }
    </Container>
  );
}

export default App;
